window.onload = async ()=>{
    let jsondata = [];
   
   
     
        await fetch('./map.php') 
        .then(response => response.json())
        .then((data) => {
              jsondata = data;
        });

         console.log(jsondata);

   var map = L.map('mapon-setMap').setView([0, 0], 13); 
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
    var greenIcon = L.icon({
      iconUrl: './assets/marker.png',
      shadowUrl: './assets/markerShadow.png',
  
      iconSize:     [38, 85], // size of the icon
      shadowSize:   [95, 50], // size of the shadow
      iconAnchor:   [5, 54], // point of the icon which will correspond to marker's location
      shadowAnchor: [5, 40],  // the same for the shadow
      popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
  });

    var latlngs = [];
    //  var markers = [];
  for (let pont = 0; pont < jsondata.length; pont++) {
      const element = jsondata[pont];
    //   console.log('try:: ',element.lng); 
      latlngs.push([element.lng, element.lat]);
    //    markers.push( element.lat , element.lng );
       L.marker([element.lng, element.lat], {
        title: element.address,
        draggable:false,
        icon: greenIcon
        }).bindPopup("<span>"+ element.address +"</span><br /><span>parada: "+ element.start +"</span>").addTo(map);
  }

 
  

 var polyline = L.polyline(latlngs, {color: 'blue'}).addTo(map);

// zoom the map to the polyline
map.fitBounds(polyline.getBounds());




// var myLatlng = new google.maps.LatLng(-34.397, 150.644);
// var mapOptions = {
//   zoom: 2,
//   center: myLatlng,
//   mapTypeId: 'roadmap'
// };
// var map = new google.maps.Map(document.getElementById('mapon-setMap'), mapOptions);

  
 

//                 for (let pont = 0; pont < jsondata.length; pont++) {
//                     const element = jsondata[pont]; 
//                     // latlngs.push([element.lng, element.lat]); 
//                 new google.maps.Marker({
//                     position: { lat: element.lng, lng: element.lat },
//                     title: "<h1>hola mund</h1>",
//                     map: map,
//                   });
//                 }



//                 const locations = [
//                     { lat: -31.56391, lng: 147.154312 },
//                     { lat: -33.718234, lng: 150.363181 },
//                     { lat: -33.727111, lng: 150.371124 },
//                     { lat: -33.848588, lng: 151.209834 },
//                     { lat: -33.851702, lng: 151.216968 },
//                     { lat: -34.671264, lng: 150.863657 },
//                     { lat: -35.304724, lng: 148.662905 },
//                     { lat: -36.817685, lng: 175.699196 },
//                     { lat: -36.828611, lng: 175.790222 },
//                     { lat: -37.75, lng: 145.116667 },
//                     { lat: -37.759859, lng: 145.128708 },
//                     { lat: -37.765015, lng: 145.133858 },
//                     { lat: -37.770104, lng: 145.143299 },
//                     { lat: -37.7737, lng: 145.145187 },
//                     { lat: -37.774785, lng: 145.137978 },
//                     { lat: -37.819616, lng: 144.968119 },
//                     { lat: -38.330766, lng: 144.695692 },
//                     { lat: -39.927193, lng: 175.053218 },
//                     { lat: -41.330162, lng: 174.865694 },
//                     { lat: -42.734358, lng: 147.439506 },
//                     { lat: -42.734358, lng: 147.501315 },
//                     { lat: -42.735258, lng: 147.438 },
//                     { lat: -43.999792, lng: 170.463352 },
//                     ];

//                     // const myLatLng = { lat: -25.363, lng: 131.044 };
               
                    
//                 // const markers = locations.map((location, i) => {
//                 //     return new google.maps.Marker({
//                 //       position: location,
//                 //     //   label: 'labels[i % labels.length]',
//                 //     });
                   
//                 //   });

//                 //   new MarkerClusterer(map, markers, {
//                 //     imagePath:
//                 //       "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m",
//                 //   });
              
               
                
//                 poly = new google.maps.Polyline({
//                     strokeColor: "red",
//                     strokeOpacity: 1.0,
//                     strokeWeight: 3,
//                   });
//                   poly.setMap(map);

                  
//                   // Add a listener for the click event
//                   map.addListener("click", addLatLng);
             
                
//                 // Handles click events on a map, and adds a new point to the Polyline.
//                 function addLatLng(event) {
//                   const path = poly.getPath();
//                   // Because path is an MVCArray, we can simply append a new coordinate
//                   // and it will automatically appear.
//                   console.log(event);
//                   path.push(event.latLng);
//                   // Add a new marker at the new plotted point on the polyline.
//                   new google.maps.Marker({
//                     position: event.latLng,
//                     title: "#" + path.getLength(),
//                     map: map,
//                   });
//                    }
 
        }
 