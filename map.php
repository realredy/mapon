<?php
use Mapon\MaponApi;
require './class/coneccion.php';
require './class/getdatatime.php'; 

$coneccion = new Coneccion(); 
$confirmConcetion = $coneccion->database_conection();
 if($confirmConcetion){ 
  $getdata = new Datatime();
  $suplydata = $getdata->getTime($confirmConcetion);

      $res = $suplydata->setFetchMode(PDO::FETCH_ASSOC); 
    foreach( $suplydata->fetchAll() as $k=>$v) { 
          
            if(isset($v['start']) && isset($v['end'])){
            $dataStart = $v['start'] . 'T12:35:23Z';
            $dataEnd = $v['end'] . 'T12:35:23Z';
            //   echo 'empieza en : ' . $dataStart . 'Finaliza en: ' . $dataEnd;
            }else{
            $dataStart = '2021-01-01T12:35:23Z';
            $dataEnd = '2021-01-20T12:35:23Z';
            }
    }
 }



include __DIR__ . './vendor/autoload.php';

date_default_timezone_set('GMT');

$apiKey = '2ab183444385fa5024dcedece4ed0f4c0be4cb06';
$apiUrl = 'https://mapon.com/api/v1/';

$api = new MaponApi($apiKey, $apiUrl);

$result = null;

try {
    $result = $api->get('route/list', array(
        'from' => $dataStart,
        'till' => $dataEnd,
        'include' => array('polyline')
    ));
} catch (\Mapon\ApiException $e) {

    header('Location: ../mapon/?Message=" Error! Code: ' . $e->getCode() .  'Message:'  . 
    $e->getMessage() .  "'" );  
}
   $datai = array();
if ($result && $result->data) {
    foreach ($result->data->units as $unit_id => $unit_data) {
        // echo "Unit: " . $unit_id . "\n";
        foreach ($unit_data->routes as $route) {
            if ($route->type == 'stop') {
                 $data = array('start'=>$route->start->time, 'end'=>$route->end->time,'address'=>$route->start->address,
                               'lng' => $route->start->lng,'lat' => $route->start->lat);
                 array_push($datai, $data);
                 
            }
        }
    }
}
$myJSON = json_encode($datai);

echo $myJSON;